ActiveAdmin.register Item do
  permit_params :size, :color, :style_id, :status

  form title: 'New Item' do |f|
    f.inputs 'Details' do
      f.input :size
      f.input :color
      f.input :status
      f.input :style
      f.actions
    end
  end
end
