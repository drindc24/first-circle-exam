$(document).ready ->
  $.noConflict()
  if $('#clearance-batch')
    batchId = $('#clearance-batch').data('batch-id')
    $('#clearance-batch').DataTable
      'ajax':
        'url': "/api/v1/clearance_batches/#{batchId}"
        'dataSrc': (response) ->
            response.items
      'columns': [
        { 'data': 'id', width: '200', className: 'text-center' }
        { 'data': 'name', width: '300', className: 'text-center' }
        { 'data': 'style', width: '200', className: 'text-center' }
        { 'data': 'size', width: '200', className: 'text-center' }
        { 'data': 'color', width: '200', className: 'text-center' }
        { 'data': 'price_sold', width: '200', className: 'text-center' }
      ]
    return