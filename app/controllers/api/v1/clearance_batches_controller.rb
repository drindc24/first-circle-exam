class Api::V1::ClearanceBatchesController < ApplicationController
  def show
    @batch = ClearanceBatch.find(params[:id])

    render json: @batch, serializer: ClearanceBatchSerializer
  end
end