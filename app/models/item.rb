class Item < ApplicationRecord
  CLEARANCE_PRICE_PERCENTAGE  = BigDecimal.new("0.75")
  MINIMUM_PRICE_OTHERS  = BigDecimal.new("2.00")
  MINIMUM_PRICE_DRESS_AND_PANTS  = BigDecimal.new("5.00")

  belongs_to :style
  belongs_to :clearance_batch

  enum status: [:sellable, :clearanced]
  enum size: [:XS, :S, :M, :L, :XL, :XXL ]
  enum color: [:purple, :blue, :red, :white, :orange, :green, :navy, :black, :turquoise]

  def clearance!
    update_attributes!(status: 'clearanced', 
                       price_sold: sale_price)
  end

  private

  def sale_price
    value = style.wholesale_price * CLEARANCE_PRICE_PERCENTAGE
    value < minimum_price ? minimum_price : value
  end

  def minimum_price
    style.dress? || style.pants? ? MINIMUM_PRICE_DRESS_AND_PANTS : MINIMUM_PRICE_OTHERS
  end
end
