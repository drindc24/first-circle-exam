class Style < ApplicationRecord
  self.inheritance_column = :_type_disabled
  has_many :items

  enum type: [:sweater, :top, :dress, :pants, :scarf]
end
