class ClearanceBatchSerializer < ActiveModel::Serializer
  attributes :id

  has_many :items, serializer: ItemSerializer
end
