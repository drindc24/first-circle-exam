class ItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :size, :color, :status, :price_sold, :style

  def style
    object.style.type
  end

  def name
    object.style.name
  end
end
