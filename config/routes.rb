Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :clearance_batches, only: [:index, :create, :show]

  namespace :api, format: :json do
    namespace :v1 do
      resources :clearance_batches, only: [:show]
    end
  end
  root to: "clearance_batches#index"
end
