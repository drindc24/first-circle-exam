class ChangeStyleType < ActiveRecord::Migration[5.1]
  def up
    change_column :styles, :type, :integer
  end

  def down
    change_column :styles, :type, :string
  end
end
