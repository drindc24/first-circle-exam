class ChangeItemStringAttrs < ActiveRecord::Migration[5.1]
  def up
    change_column :items, :size, :integer
    change_column :items, :color, :integer
    change_column :items, :status, :integer
  end

  def down
    change_column :items, :size, :string
    change_column :items, :color, :string
    change_column :items, :status, :string
  end
end
