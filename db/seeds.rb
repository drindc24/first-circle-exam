# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def make_items(style,color,sizes: {"XS"=>10, "S"=>10, "M"=>10, "L"=>10, "XL"=>10, "XXL"=>10})
  sizes.each do |size,count|
    count.times do
      size = Item.sizes.keys.sample if size == 'ANY'
      Item.create(color: color, size: size, status: 'sellable', style: style)
    end
  end
end

Style.create(name: "Abrianna Lightweight Knit Cardigan",
             type: "sweater",  wholesale_price: 10, retail_price: 60).tap { |style|
  make_items(style,"purple")
  make_items(style,"blue")
}
Style.create(name: "Bryan Short-Sleeve Open-Front Cardigan",
             type: "sweater",  wholesale_price: 15, retail_price: 60).tap { |style|
  make_items(style,"red")
  make_items(style,"blue")
}
Style.create(name: "Vicky Colorblock Trim Silk Blouse",
             type: "top",      wholesale_price: 13, retail_price: 45).tap { |style|
  make_items(style,"white")
}
Style.create(name: "Collegno Diamond Print Drawstring Waist Dress",
             type: "dress",    wholesale_price: 18, retail_price: 80).tap { |style|
  make_items(style,"orange")
  make_items(style,"green")
}
Style.create(name: "Pomelo Critter Print Maxi Dress",
             type: "dress",    wholesale_price: 6, retail_price: 80).tap { |style|
  make_items(style,"orange")
  make_items(style,"green")
}
Style.create(name: "Leah Straight Leg Cuffed Jean",
             type: "pants",    wholesale_price: 34, retail_price: 90).tap { |style|
  make_items(style,"navy")
  make_items(style,"black")
}
Style.create(name: "Henry Birds on Branch Infinity Scarf",
             type: "scarf",    wholesale_price: 2, retail_price: 30).tap { |style|
  make_items(style,"turquoise", sizes: { 'ANY' => 20 })
}
Style.create(name: "Blue Camo Print Boyfriend Jeans",
             type: "pants",    wholesale_price: 5, retail_price: 70).tap { |style|
  make_items(style,"navy")
}
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?