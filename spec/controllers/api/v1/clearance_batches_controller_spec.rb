require 'rails_helper'

RSpec.describe Api::V1::ClearanceBatchesController, type: :controller do
  let!(:admin){ create(:admin_user) }

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:admin_user]
    sign_in admin
  end

  describe '#show' do
    let!(:batch){ create(:clearance_batch) }
    let!(:item){ create(:item, clearance_batch: batch) }

    before do
      get :show, params: { id: batch.id }
    end

    it 'should return the correct items' do
      response_body = JSON.parse response.body

      expect(response_body["items"].first["id"]).to eq batch.id
    end
  end
end