require 'rails_helper'

RSpec.describe ClearanceBatchesController, type: :controller do
  let!(:admin){ create(:admin_user) }
  let(:batch1){ create(:clearance_batch) }
  let(:batch2){ create(:clearance_batch) }

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:admin_user]
    sign_in admin
  end

  describe "#index" do
    before do
      get :index
    end

    it 'should return all the batches' do
      expect(assigns[:clearance_batches]).to contain_exactly(batch1, batch2)
    end
  end

  describe "#show" do
    before do
      get :show, params: { id: batch1.id }
    end

    it 'should return the batch referenced by the id param' do
      expect(assigns[:clearance_batch]).to eq batch1
    end
  end
end