FactoryBot.define do
  factory :admin_user do
    email { "admin@example.com" }
    password { "password" }
  end

  factory :clearance_batch

  factory :item do
    association :style
    color { "blue" }
    size { "M" }
    status { "sellable" }
  end

  factory :style do
    wholesale_price { rand(100) }

    trait :sweater do
      type { 'sweater' }
    end

    trait :top do
      type { 'top' }
    end

    trait :dress do
      type { 'dress' }
    end

    trait :pants do
      type { 'pants' }
    end

    trait :scarf do
      type { 'scarf' }
    end
  end
end
