require 'rails_helper'

describe Item do
  describe "#perform_clearance!" do
    context "#less_than_minimum" do
      context "#pants_and_dresses" do
        context "#pants" do
          let(:wholesale_price_pants) { 4 }
          let(:pants_style){ create(:style, :pants, wholesale_price: wholesale_price_pants) }
          let(:pants) { create(:item, style: pants_style) }

          before do
            pants.clearance!
            pants.reload
          end

          it 'should mark the item status as clearanced' do
            expect(pants.status).to eq('clearanced')
          end

          it 'should set the price_sold to the minimum for dress/pants' do
            expect(pants.price_sold).to eq(Item::MINIMUM_PRICE_DRESS_AND_PANTS)
          end
        end

        context "#dress" do
          let(:wholesale_price_dress) { 3 }
          let(:dress_style){ create(:style, :dress, wholesale_price: wholesale_price_dress) }
          let(:dress) { create(:item, style: dress_style) }

          before do
            dress.clearance!
            dress.reload
          end

          it 'should mark the item status as clearanced' do
            expect(dress.status).to eq('clearanced')
          end

          it 'should set the price_sold to the minimum for dress/pants' do
            expect(dress.price_sold).to eq(Item::MINIMUM_PRICE_DRESS_AND_PANTS)
          end
        end
      end

      context "#other_types" do
        context "#sweater" do
          let(:wholesale_price) { 1 }
          let(:sweater_style){ create(:style, :sweater, wholesale_price: wholesale_price) }
          let(:sweater) { create(:item, style: sweater_style) }

          before do
            sweater.clearance!
            sweater.reload
          end

          it 'should mark the item status as clearanced' do
            expect(sweater.status).to eq('clearanced')
          end

          it 'should set the price_sold to the minimum price for sweaters/scarves/tops' do
            expect(sweater.price_sold).to eq(Item::MINIMUM_PRICE_OTHERS)
          end
        end

        context "#scarf" do
          let(:wholesale_price) { 2 }
          let(:scarf_style){ create(:style, :scarf, wholesale_price: wholesale_price) }
          let(:scarf) { create(:item, style: scarf_style) }

          before do
            scarf.clearance!
            scarf.reload
          end

          it 'should mark the item status as clearanced' do
            expect(scarf.status).to eq('clearanced')
          end

          it 'should set the price_sold to the minimum price for sweaters/scarves/tops' do
            expect(scarf.price_sold).to eq(Item::MINIMUM_PRICE_OTHERS)
          end
        end

        context "#tops" do
          let(:wholesale_price) { 1.5 }
          let(:top_style){ create(:style, :scarf, wholesale_price: wholesale_price) }
          let(:top) { create(:item, style: top_style) }

          before do
            top.clearance!
            top.reload
          end

          it 'should mark the item status as clearanced' do
            expect(top.status).to eq('clearanced')
          end

          it 'should set the price_sold to the minimum price for sweaters/scarves/tops' do
            expect(top.price_sold).to eq(Item::MINIMUM_PRICE_OTHERS)
          end
        end
      end
    end

    context "#more_than_minimum" do
      context "#pants_and_dresses" do
        context "#pants" do
          let(:wholesale_price) { 20 }
          let(:pants_style){ create(:style, :sweater, wholesale_price: wholesale_price) }
          let(:pants) { create(:item, style: pants_style) }

          before do
            pants.clearance!
            pants.reload
          end

          it 'should mark the item status as clearanced' do
            expect(pants.status).to eq('clearanced')
          end

          it "should set the price_sold as 75% of the wholesale_price" do
            expect(pants.price_sold).to eq(BigDecimal.new("15.00"))
          end
        end

        context "#dress" do
          let(:wholesale_price_dress) { 30 }
          let(:dress_style){ create(:style, :dress, wholesale_price: wholesale_price_dress) }
          let(:dress) { create(:item, style: dress_style) }

          before do
            dress.clearance!
            dress.reload
          end

          it 'should mark the item status as clearanced' do
            expect(dress.status).to eq('clearanced')
          end

          it "should set the price_sold as 75% of the wholesale_price" do
            expect(dress.price_sold).to eq(BigDecimal.new("22.50"))
          end
        end
      end

      context "#other_types" do
        context "#sweater" do
          let(:wholesale_price) { 100 }
          let(:sweater_style){ create(:style, :sweater, wholesale_price: wholesale_price) }
          let(:sweater) { create(:item, style: sweater_style) }

          before do
            sweater.clearance!
            sweater.reload
          end

          it 'should mark the item status as clearanced' do
            expect(sweater.status).to eq('clearanced')
          end

          it "should set the price_sold as 75% of the wholesale_price" do
            expect(sweater.price_sold).to eq(BigDecimal.new("75.00"))
          end
        end

        context "#scarf" do
          let(:wholesale_price) { 40 }
          let(:scarf_style){ create(:style, :scarf, wholesale_price: wholesale_price) }
          let(:scarf) { create(:item, style: scarf_style) }

          before do
            scarf.clearance!
            scarf.reload
          end

          it 'should mark the item status as clearanced' do
            expect(scarf.status).to eq('clearanced')
          end

          it "should set the price_sold as 75% of the wholesale_price" do
            expect(scarf.price_sold).to eq(BigDecimal.new("30.00"))
          end
        end

        context "#tops" do
          let(:wholesale_price) { 50 }
          let(:top_style){ create(:style, :scarf, wholesale_price: wholesale_price) }
          let(:top) { create(:item, style: top_style) }

          before do
            top.clearance!
            top.reload
          end

          it 'should mark the item status as clearanced' do
            expect(top.status).to eq('clearanced')
          end

          it "should set the price_sold as 75% of the wholesale_price" do
            expect(top.price_sold).to eq(BigDecimal.new("37.50"))
          end
        end
      end
    end
  end
end
