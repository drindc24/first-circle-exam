module FeatureSpecHelpers
  def sign_in
    create(:admin_user)
    visit "/"
    fill_in "admin_user_email", with: "admin@example.com"
    fill_in "admin_user_password", with: "password"
    click_button "Login"
  end
end